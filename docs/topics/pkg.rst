================
Package Building
================

While pop-build can deliver single binaries, it is often optimal to deliver
system packages as well. Adding a `pkg` section to the config will allow
pop-build to create packages for multiple Linux distributions (Windows
Installer is being worked on).

Pre-requisites
==============

Pop-build relies on the venerable `fpm` system to build packages on *nix*
platforms, as well as distribution package management software. Install
`fpm` as a system package as well as any package build tools that are required.

Configuration
=============

To build a package simply add a section to your config that defines the package
needs. SystemD files, execution scripts, and config files, can all be specified
for the package build. Pop-build will put them in the right place and flag
them as the correct types of files.

.. code-block:: yaml

    pop_build:
      pkg:
        config:
          /etc/pb/pb.conf: pkgfiles/config/pb.conf
        systemd:
            - pkgfiles/systemd/test.service
        scripts:
            - pkgfiles/scripts/pb-wrap

Under pkg you can specify the location to set configuration files. The location
to save the configuration file is followed by where the configuration file
can be found, relative to the root of the repository.

Specific Location Files
-----------------------

The `systemd`, `sysv`, and `scripts` options take lists of files. Those files
are then placed inside of the correct location.

Command Line Options
====================

The package builder is only activated if the correct command line flags are
passed. This allows for the config file to be used for other options without
attempting to build the package.

To build the package, set the `pkg-tgt` This is the target OS to build the
package for. Packages should always be build on the intended target OS!

Therefore, a package will only be built if the `--pkg-tgt` option is passed
to pop-build:

.. code-block:: bash

    pop-build -c config.yml --pkg-tgt rhel8

The Package Builder
===================

The package build system is, of course, pluggable. The default package builder
is through the `fpm` plugin. If another plugin is desired pass it in via the
`--pkg-builder` option.
